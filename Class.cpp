#include "Class.h"


Class::Class(std::string set_label, int bag_length) : label(set_label), feature_vectors(0, std::vector<float>(bag_length, 0)), numberOfDocuments(0), numberOfWords(0) {}